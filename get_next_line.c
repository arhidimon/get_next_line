/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 17:10:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/11/28 17:10:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdlib.h>
#include "libft/includes/libft.h"

#include "stdio.h"//rm

//t_file	initialize(fd)
//{
//	t_filelist filelist;
//
//
//	filelist.count = 1;
//	filelist.files = (t_file *)malloc(sizeof(t_file));
//	filelist.files->fd = fd;
//	filelist.files->last_call = 0;
//	filelist.files->next = NULL;
//	filelist.files->lines.next = NULL;
//	return (filelist);
//	t_file *file;
//
//
//	file = (t_file *)malloc(sizeof(t_file));
//	file->fd = fd;
//	file->last_call = 0;
//	file->next = NULL;
//	file->lines.next = NULL;
//	if	(read_once(fd, file) <= 0)
//		file->is_end = 1;
//	else
//		file->is_end = 0;
//return (file);
//}
//
//size_t		find_eol(int fd, t_filelist *file)
//{
//	size_t	counter;
//	t_line	*line;
//
//	while (file && file->fd != fd)
//		file = file->next;
//	line = file->lines;
//	counter = 0;
//	while (line)
//	{
//		from = file->last_call;
//		while (line->linepart[from] && line->linepart[from] != '\n' && from < BUFF_SIZE)
//			from++;
//		counter += from;
//		if (linepart[from] == '\n')
//			return (counter);
//		line = line->next;
//	}
//	return (counter);
//}
//
//int			read_once(int fd, t_filelist *file)
//{
//	t_line	*line;
//
//	while (file && file->fd != fd)
//		file = file->next;
//	if (file == NULL)
//		file = (t_file *)malloc(sizeof(t_file));
//	if (file == NULL)
//		return (-1);
//	line = file->lines;
//	while (line)
//		line = line->next;
//	line = (t_line *)malloc(sizeof(t_line));
//	if (line == NULL)
//		return (-1);
//	line->linepart = (char*)malloc(sizeof(char)*BUFF_SIZE);
//	if (line->linepart == NULL)
//		return (-1);
//	return (read(fd, line->linepart, BUFF_SIZE));
//}
//
//char			*allocate_line(int fd, t_filelist *file)
//{
//	size_t				wr;
//	size_t				index;
//	size_t				i;
//	char 				*str;
//
//	while (file && file->fd != fd)
//		file = file->next;
//	if (file == NULL)
//		return (NULL);
//	index = find_eol(fd, file);
//	if(!(str = (char*)malloc(index + 1)))
//		return (NULL);
//	while (index > BUFF_SIZE)
//	{
//		i = file->last_call;
//		str[index] = '\0';
//		while (i < BUFF_SIZE && i < index)
//			str[wr++] = file->lines->linepart[i++];
//		file->last_call = 0;
//		index -= BUFF_SIZE;//add free!!!
//		file->lines = file->lines->next;
//	}
//	if (find_eol(fd, file) == BUFF_SIZE)
//	{
//		wr = read_once(fd, filelist);
//		return (allocate_line(fd,file));
//	}
//	return (str);
//
//}
//
//
//int			get_next_line(const int fd, char **line)
//{
//	//static t_filelist	files = initialize(fd);
//	static t_file	filelist = initialize(fd);
//	size_t				wr;
//	size_t				index;
//	char 				*str;
//
//	index = find_eol(fd, &filelist);
//	if (index < BUFF_SIZE)
//	{
//		str = (char*)malloc(index + 1);
//		wr = 0;
//		str[index] = '\0';
//		while (wr < index)
//			str[index] = ma
//	}
//	wr = read_once(fd, filelist);
//	*line = allocate_line(fd,file);
//	return (1);
//
//
//
//
//
//	return (0);
//}
int 		str_append(int fd, char **s)
{
    char    buff[BUFF_SIZE+1];
    char	*stemp;
    int 	wr;

    wr = read(fd, buff, BUFF_SIZE);
    if (!s || wr < 0)
        return (ERR_PTRISNULL);
    buff[wr] = '\0';
    if (wr == 0)
        return (REACH_EOF);

    if (!*s)
    {
        *s = ft_strdup(buff);
        return (NO_ERROR);
    }
    if (!(stemp = ft_strjoin(*s, buff)))
        return (ERR_MALLOC);
    free(*s);
    *s = stemp;
    return (NO_ERROR);
}

int 		str_remove(int fd, char **s, char **sout)
{
	size_t	i;
	size_t	j;
    char	*spos;
	char	*st;

	if (!s || !*s)
		return (ERR_PTRISNULL);
    if (!(spos = ft_strchr(*s, '\n')))
    {
        if(str_append(fd, s) == REACH_EOF)
            return (REACH_EOF);
        return (str_remove(fd, s, sout));
    }
    j = spos - *s;

    i = ft_strlen(*s);
    printf("j = %i i = %i\n",(int)j,(int)i);
    *sout = ft_strsub(*s, 0, j);
    st = ft_strsub(*s, j+1, i - j - 1);
    free(*s);
    *s = st;
    return (NO_ERROR);
}

t_file		*search_fd(t_file **file, int fd)
{
    t_file *f;

	if (*file == NULL)
	{
		if (!(*file = (t_file*)malloc(sizeof(t_file))))
			return (NULL);
        (*file)->fd = fd;
        (*file)->next = NULL;
        (*file)->is_end = FALSE;
        (*file)->str = NULL;
		return (*file);
	}
    f = *file;
	while (f && f->next && f->fd != fd)
		f = f->next;
	if (f->fd == fd)
		return (f);
	if (!(f->next = (t_file*)malloc(sizeof(t_file))))
		return (NULL);
	f->next->fd = fd;
	f->next->next = NULL;
	f->next->is_end = FALSE;
	f->next->str = NULL;
	return (f->next);
}

int			get_next_line(const int fd, char **line)
{
	static t_file	*file = NULL;
	t_file			*cf;
    int             rcode;

    //printf(" fileptr%p\n",file);
	cf = search_fd(&file, fd);
    if (cf->is_end == TRUE)
        return (0);
	if (cf->str == NULL)
    {
        if ((rcode = str_append(cf->fd, &cf->str)) < 0)
            return (-1);
        else if (rcode == REACH_EOF)
        {
            file->is_end = TRUE;
            return (0);
        }
    }
    printf("before|%s|\n",cf->str);
    rcode = str_remove(cf->fd, &(cf->str),line);
    if (rcode < 0)
        return (-1);
    if (rcode == REACH_EOF)
    {
        file->is_end = TRUE;
        return (0);
    }
    printf("after|%s|\n",cf->str);
    return (1);

}
